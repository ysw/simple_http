package repository

import (
	"simple-http/apps/models"
	"simple-http/helpers"
	"strconv"
)

func GetUsers(page int) ([]models.User, error) {
	url := helpers.BasicUrl + "/api/users?page=" + strconv.Itoa(page)

	headers := map[string]string{
		"Content-Type": "application/json",
	}

	resp, err := helpers.GetRequest(url, "GET", nil, headers)
	if err != nil {
		return nil, err
	}

	if err := helpers.ParseResponseBody(resp, &models.UsersResponse); err != nil {
		return nil, err
	}

	return models.UsersResponse.Data, nil
}
