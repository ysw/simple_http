package models

var UsersResponse struct {
	Page      int    `json:"page"`
	PerPage   int    `json:"per_page"`
	Total     int    `json:"total"`
	TotalPage int    `json:"total_pages"`
	Data      []User `json:"data"`
}
