package routes

import (
	"github.com/gin-gonic/gin"
	"simple-http/apps/controller"
	"simple-http/apps/middleware"
)

func Routes(router *gin.Engine) {
	userGroup := router.Group("/api")
	{
		userGroup.Use(middleware.BasicAuth())
		userGroup.GET("/users", controller.GetUsers)
	}
}
