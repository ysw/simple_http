package middleware

import (
	"github.com/gin-gonic/gin"
)

// ini basic auth
func BasicAuth() gin.HandlerFunc {
	return gin.BasicAuth(gin.Accounts{
		"username": "password",
	})
}
