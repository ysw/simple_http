package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"simple-http/apps/repository"
	"strconv"
)

func GetUsers(c *gin.Context) {
	pageStr := c.Query("page")
	if pageStr == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Page parameter is required"})
		return
	}

	page, err := strconv.Atoi(pageStr)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid page parameter"})
		return
	}

	users, err := repository.GetUsers(page)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch users"})
		return
	}

	c.JSON(http.StatusOK, users)
}
