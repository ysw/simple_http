package main

import (
	"github.com/gin-gonic/gin"
	"simple-http/apps/routes"
)

func main() {
	r := gin.Default()

	// Initialize routes
	routes.Routes(r)
	//port default adalah 8080
	r.Run(":8080")
}
